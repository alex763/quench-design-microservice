package routes

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"quench-design-microservice/app/controllers"
	"quench-design-microservice/app/models"
	"quench-design-microservice/platform/database"

	"github.com/gofiber/adaptor/v2"
	"github.com/gofiber/fiber/v2"
)

type Client struct {
	name   string
	events chan *Channel
}
type Channel struct {
	// User  uint
	Ideas []models.Idea
}

// PublicRoutes func for describe group of public routes.
func PublicRoutes(a *fiber.App) {
	// Create routes group.
	route := a.Group("/api/v2")

	// Routes for GET method:
	route.Get("/ideas/all", controllers.GetIdeas)          // get list of all ideas
	route.Get("/idea/:id", controllers.GetIdea)            // get one idea by ID
	route.Get("/token/new", controllers.GetNewAccessToken) // create a new access tokens

	route.Get("/sse", adaptor.HTTPHandler(handler(dashboardHandler)))
}

func handler(f http.HandlerFunc) http.Handler {
	return http.HandlerFunc(f)
}

func dashboardHandler(w http.ResponseWriter, r *http.Request) {

	client := &Client{name: r.RemoteAddr, events: make(chan *Channel, 10)}

	go updateDashboard(client)

	// w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	w.Header().Set("Content-Type", "text/event-stream")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")

	timeout := time.After(1 * time.Second)

	select {
	case ev := <-client.events:
		var buf bytes.Buffer
		enc := json.NewEncoder(&buf)
		enc.Encode(ev)
		fmt.Fprintf(w, "data: %v\n\n", buf.String())
		fmt.Printf("data: %v\n", buf.String())
	case <-timeout:
		fmt.Fprintf(w, ": nothing to sent\n\n")
	}

	if f, ok := w.(http.Flusher); ok {
		f.Flush()
	}
}

func updateDashboard(client *Client) {

	db, err := database.OpenDBConnection()
	if err != nil {
		println(err)
	}
	Ideas, err := db.GetIdeas()
	if err != nil {
		println(err)
	}
	for {
		db := &Channel{
			Ideas: Ideas,
		}

		client.events <- db
	}
}
