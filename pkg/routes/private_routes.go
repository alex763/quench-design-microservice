package routes

import (
	"quench-design-microservice/pkg/middleware"

	"quench-design-microservice/app/controllers"

	"github.com/gofiber/fiber/v2"
)

// PrivateRoutes func for describe group of private routes.
func PrivateRoutes(a *fiber.App) {
	// Create routes group.
	route := a.Group("/api/v2")

	// Routes for POST method:
	route.Post("/idea", middleware.JWTProtected(), controllers.CreateIdea) // create a new idea

	// Routes for PUT method:
	route.Put("/idea", middleware.JWTProtected(), controllers.UpdateIdea) // update one idea by ID

	// Routes for DELETE method:
	route.Delete("/idea", middleware.JWTProtected(), controllers.DeleteIdea) // delete one idea by ID
}
