module quench-design-microservice

go 1.16

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/arsmn/fiber-swagger/v2 v2.17.0
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/go-openapi/jsonreference v0.19.6 // indirect
	github.com/go-openapi/swag v0.19.15 // indirect
	github.com/go-playground/validator/v10 v10.9.0
	github.com/gofiber/adaptor/v2 v2.1.16
	github.com/gofiber/fiber/v2 v2.24.0
	github.com/gofiber/jwt/v2 v2.2.6
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/google/uuid v1.3.0
	github.com/jackc/pgx/v4 v4.13.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/joho/godotenv v1.3.0
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/stretchr/testify v1.7.0
	github.com/swaggo/swag v1.7.1
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d // indirect
	golang.org/x/sys v0.0.0-20210823070655-63515b42dcdf // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.5 // indirect
)
