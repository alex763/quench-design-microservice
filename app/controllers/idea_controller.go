package controllers

import (
	"time"

	"quench-design-microservice/app/models"
	"quench-design-microservice/pkg/utils"
	"quench-design-microservice/platform/database"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

// GetIdeas func gets all exists ideas.
// @Description Get all exists ideas.
// @Summary get all exists ideas
// @Tags Ideas
// @Accept json
// @Produce json
// @Success 200 {array} models.Idea
// @Router /api/v2/ideas/all [get]
func GetIdeas(c *fiber.Ctx) error {
	// Create database connection.
	db, err := database.OpenDBConnection()
	if err != nil {
		// Return status 500 and database connection error.
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	// Get all ideas.
	Ideas, err := db.GetIdeas()

	if err != nil {
		// Return, if ideas not found.
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
			"error": err.Error(),
			"msg":   "ideas were not found",
			"count": 0,
			"ideas": nil,
		})
	}

	// Return status 200 OK.
	return c.JSON(fiber.Map{
		"error": false,
		"msg":   nil,
		"count": len(Ideas),
		"ideas": Ideas,
	})
}

// GetIdea func gets idea by given ID or 404 error.
// @Description Get idea by given ID.
// @Summary get idea by given ID
// @Tags Idea
// @Accept json
// @Produce json
// @Param id path string true "Idea ID"
// @Success 200 {object} models.Idea
// @Router /api/v2/idea/{id} [get]
func GetIdea(c *fiber.Ctx) error {
	// Catch idea ID from URL.
	id, err := uuid.Parse(c.Params("id"))
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	// Create database connection.
	db, err := database.OpenDBConnection()
	if err != nil {
		// Return status 500 and database connection error.
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	// Get idea by ID.
	idea, err := db.GetIdea(id)
	if err != nil {
		// Return, if idea not found.
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
			"error": true,
			"msg":   "idea with the given ID is not found",
			"idea":  nil,
		})
	}

	// Return status 200 OK.
	return c.JSON(fiber.Map{
		"error": false,
		"msg":   nil,
		"idea":  idea,
	})
}

// CreateIdea func for creates a new idea.
// @Description Create a new idea.
// @Summary create a new idea
// @Tags Idea
// @Accept json
// @Produce json
// @Param title body string true "Title"
// @Param author body string true "Author"
// @Param idea_attrs body models.IdeaAttrs true "Idea attributes"
// @Success 200 {object} models.Idea
// @Security ApiKeyAuth
// @Router /api/v2/idea [post]
func CreateIdea(c *fiber.Ctx) error {
	// Get now time.
	now := time.Now().Unix()

	// Get claims from JWT.
	claims, err := utils.ExtractTokenMetadata(c)
	if err != nil {
		// Return status 500 and JWT parse error.
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	// Set expiration time from JWT data of current idea.
	expires := claims.Expires

	// Checking, if now time greather than expiration from JWT.
	if now > expires {
		// Return status 401 and unauthorized error message.
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"error": true,
			"msg":   "unauthorized, check expiration time of your token",
		})
	}

	// Create new Idea struct
	idea := &models.Idea{}

	// Check, if received JSON data is valid.
	if err := c.BodyParser(idea); err != nil {
		// Return status 400 and error message.
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	// Create database connection.
	db, err := database.OpenDBConnection()
	if err != nil {
		// Return status 500 and database connection error.
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	// Create a new validator for a Idea model.
	validate := utils.NewValidator()

	// Set initialized default data for idea:
	idea.ID = uuid.New()
	idea.CreatedAt = time.Now()
	idea.IdeaStatus = 1 // 0 == draft, 1 == active

	// Validate idea fields.
	if err := validate.Struct(idea); err != nil {
		// Return, if some fields are not valid.
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": true,
			"msg":   utils.ValidatorErrors(err),
		})
	}

	// Delete idea by given ID.
	if err := db.CreateIdea(idea); err != nil {
		// Return status 500 and error message.
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	// Return status 200 OK.
	return c.JSON(fiber.Map{
		"error": false,
		"msg":   nil,
		"idea":  idea,
	})
}

// UpdateIdea func for updates idea by given ID.
// @Description Update idea.
// @Summary update idea
// @Tags Idea
// @Accept json
// @Produce json
// @Param id body string true "Idea ID"
// @Param title body string true "Title"
// @Param author body string true "Author"
// @Param idea_status body integer true "Idea status"
// @Param idea_attrs body models.IdeaAttrs true "Idea attributes"
// @Success 201 {string} status "ok"
// @Security ApiKeyAuth
// @Router /api/v2/idea [put]
func UpdateIdea(c *fiber.Ctx) error {
	// Get now time.
	now := time.Now().Unix()

	// Get claims from JWT.
	claims, err := utils.ExtractTokenMetadata(c)
	if err != nil {
		// Return status 500 and JWT parse error.
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	// Set expiration time from JWT data of current idea.
	expires := claims.Expires

	// Checking, if now time greather than expiration from JWT.
	if now > expires {
		// Return status 401 and unauthorized error message.
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"error": true,
			"msg":   "unauthorized, check expiration time of your token",
		})
	}

	// Create new Idea struct
	idea := &models.Idea{}

	// Check, if received JSON data is valid.
	if err := c.BodyParser(idea); err != nil {
		// Return status 400 and error message.
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	// Create database connection.
	db, err := database.OpenDBConnection()
	if err != nil {
		// Return status 500 and database connection error.
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	// Checking, if idea with given ID is exists.
	foundedIdea, err := db.GetIdea(idea.ID)
	if err != nil {
		// Return status 404 and idea not found error.
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
			"error": true,
			"msg":   "idea with this ID not found",
		})
	}

	// Set initialized default data for idea:
	idea.UpdatedAt = time.Now()

	// Create a new validator for a Idea model.
	validate := utils.NewValidator()

	// Validate idea fields.
	if err := validate.Struct(idea); err != nil {
		// Return, if some fields are not valid.
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": true,
			"msg":   utils.ValidatorErrors(err),
		})
	}

	// Update idea by given ID.
	if err := db.UpdateIdea(foundedIdea.ID, idea); err != nil {
		// Return status 500 and error message.
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	// Return status 201.
	return c.SendStatus(fiber.StatusAccepted)
}

// DeleteIdea func for deletes idea by given ID.
// @Description Delete idea by given ID.
// @Summary delete idea by given ID
// @Tags Idea
// @Accept json
// @Produce json
// @Param id body string true "Idea ID"
// @Success 204 {string} status "ok"
// @Security ApiKeyAuth
// @Router /api/v2/idea [delete]
func DeleteIdea(c *fiber.Ctx) error {
	// Get now time.
	now := time.Now().Unix()

	// Get claims from JWT.
	claims, err := utils.ExtractTokenMetadata(c)
	if err != nil {
		// Return status 500 and JWT parse error.
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	// Set expiration time from JWT data of current idea.
	expires := claims.Expires

	// Checking, if now time greather than expiration from JWT.
	if now > expires {
		// Return status 401 and unauthorized error message.
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"error": true,
			"msg":   "unauthorized, check expiration time of your token",
		})
	}

	// Create new Idea struct
	idea := &models.Idea{}

	// Check, if received JSON data is valid.
	if err := c.BodyParser(idea); err != nil {
		// Return status 400 and error message.
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	// Create a new validator for a Idea model.
	validate := utils.NewValidator()

	// Validate only one idea field ID.
	if err := validate.StructPartial(idea, "id"); err != nil {
		// Return, if some fields are not valid.
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": true,
			"msg":   utils.ValidatorErrors(err),
		})
	}

	// Create database connection.
	db, err := database.OpenDBConnection()
	if err != nil {
		// Return status 500 and database connection error.
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	// Checking, if idea with given ID is exists.
	foundedIdea, err := db.GetIdea(idea.ID)
	if err != nil {
		// Return status 404 and idea not found error.
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
			"error": true,
			"msg":   "idea with this ID not found",
		})
	}

	// Delete idea by given ID.
	if err := db.DeleteIdea(foundedIdea.ID); err != nil {
		// Return status 500 and error message.
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	// Return status 204 no content.
	return c.SendStatus(fiber.StatusNoContent)
}
