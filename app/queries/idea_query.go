package queries

import (
	"quench-design-microservice/app/models"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

// IdeaQueries struct for queries from Idea model.
type IdeaQueries struct {
	*sqlx.DB
}

// GetIdeas method for getting all Ideas.
func (q *IdeaQueries) GetIdeas() ([]models.Idea, error) {
	// Define Ideas variable.
	Ideas := []models.Idea{}

	// Define query string.
	query := `SELECT * FROM Ideas WHERE idea_status = 1`

	// Send query to database.
	err := q.Select(&Ideas, query)
	if err != nil {
		// Return empty object and error.
		return Ideas, err
	}

	// Return query result.
	return Ideas, nil
}

// GetIdea method for getting one Idea by given ID.
func (q *IdeaQueries) GetIdea(id uuid.UUID) (models.Idea, error) {
	// Define Idea variable.
	Idea := models.Idea{}

	// Define query string.
	query := `SELECT * FROM Ideas WHERE id = $1`

	// Send query to database.
	err := q.Get(&Idea, query, id)
	if err != nil {
		// Return empty object and error.
		return Idea, err
	}

	// Return query result.
	return Idea, nil
}

// CreateIdea method for creating Idea by given Idea object.
func (q *IdeaQueries) CreateIdea(b *models.Idea) error {
	// Define query string.
	query := `INSERT INTO Ideas VALUES ($1, $2, $3, $4, $5, $6, $7)`

	// Send query to database.
	_, err := q.Exec(query, b.ID, b.CreatedAt, b.UpdatedAt, b.Title, b.Author, b.IdeaStatus, b.IdeaAttrs)
	if err != nil {
		// Return only error.
		return err
	}

	// This query returns nothing.
	return nil
}

// UpdateIdea method for updating Idea by given Idea object.
func (q *IdeaQueries) UpdateIdea(id uuid.UUID, b *models.Idea) error {
	// Define query string.
	query := `UPDATE Ideas SET updated_at = $2, title = $3, author = $4, Idea_status = $5, Idea_attrs = $6 WHERE id = $1`

	// Send query to database.
	_, err := q.Exec(query, id, b.UpdatedAt, b.Title, b.Author, b.IdeaStatus, b.IdeaAttrs)
	if err != nil {
		// Return only error.
		return err
	}

	// This query returns nothing.
	return nil
}

// DeleteIdea method for delete Idea by given ID.
func (q *IdeaQueries) DeleteIdea(id uuid.UUID) error {
	// Define query string.
	query := `DELETE FROM Ideas WHERE id = $1`

	// Send query to database.
	_, err := q.Exec(query, id)
	if err != nil {
		// Return only error.
		return err
	}

	// This query returns nothing.
	return nil
}
