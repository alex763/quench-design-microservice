package models

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"time"

	"github.com/google/uuid"
)

// Idea struct to describe idea object.
type Idea struct {
	ID         uuid.UUID `db:"id" json:"id" validate:"required,uuid"`
	CreatedAt  time.Time `db:"created_at" json:"created_at"`
	UpdatedAt  time.Time `db:"updated_at" json:"updated_at"`
	Title      string    `db:"title" json:"title" validate:"required,lte=255"`
	Author     string    `db:"author" json:"author" validate:"required,lte=255"`
	IdeaStatus int       `db:"idea_status" json:"idea_status" validate:"required,len=1"`
	IdeaAttrs  IdeaAttrs `db:"idea_attrs" json:"idea_attrs" validate:"required,dive"`
}

type Ideas []Idea

// IdeaAttrs struct to describe idea attributes.
type IdeaAttrs struct {
	Picture     string `json:"picture"`
	Description string `json:"description"`
	Rating      int    `json:"rating" validate:"min=1,max=10"`
}

// Value make the IdeaAttrs struct implement the driver.Valuer interface.
// This method simply returns the JSON-encoded representation of the struct.
func (b IdeaAttrs) Value() (driver.Value, error) {
	return json.Marshal(b)
}

// Scan make the IdeaAttrs struct implement the sql.Scanner interface.
// This method simply decodes a JSON-encoded value into the struct fields.
func (b *IdeaAttrs) Scan(value interface{}) error {
	j, ok := value.([]byte)
	if !ok {
		return errors.New("type assertion to []byte failed")
	}
	return json.Unmarshal(j, &b)
}
