-- Add UUID extension
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
SET TIMEZONE="Africa/Harare";

-- Create ideas table
CREATE TABLE ideas (
    id UUID DEFAULT uuid_generate_v4 () PRIMARY KEY,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW (),
    updated_at TIMESTAMP NULL,
    title VARCHAR (255) NOT NULL,
    author VARCHAR (255) NOT NULL,
    idea_status INT NOT NULL,
    idea_attrs JSONB NOT NULL
);

-- Add indexes
CREATE INDEX active_ideas ON ideas (title) WHERE idea_status = 1;