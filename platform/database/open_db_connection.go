package database

import "quench-design-microservice/app/queries"

// Queries struct for collect all app queries.
type Queries struct {
	*queries.IdeaQueries // load queries from Idea model
}

// OpenDBConnection func for opening database connection.
func OpenDBConnection() (*Queries, error) {
	// Define a new PostgreSQL connection.
	db, err := PostgreSQLConnection()
	if err != nil {
		return nil, err
	}

	return &Queries{
		// Set queries from models:
		IdeaQueries: &queries.IdeaQueries{DB: db}, // from Idea model
	}, nil
}
